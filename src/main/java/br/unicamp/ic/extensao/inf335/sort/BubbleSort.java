package br.unicamp.ic.extensao.inf335.sort;

public class BubbleSort {
	
	public static void sort(int[] vector) {
		boolean switched = true;
		int aux;
		for (int i = 0; i < vector.length - 1; i++) {
			switched = true;
			for (int j = 0; j < vector.length - 1; j++) {
				if (vector[j] > vector[j + 1]) {
					swap(vector, j);
				}
				switched = false;
			}
			if (switched) {
				break;
			}
		}
	}

	private static void swap(int[] vector, int j) {
		int aux;
		aux = vector[j];
		vector[j] = vector[j + 1];
		vector[j+1] = aux;
	}
}

