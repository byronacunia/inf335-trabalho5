package br.unicamp.ic.extensao.inf335.sort;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VectorSorter {

	public static final int VECTOR_SIZE = 20;
	public static final int CEM = 100;

	private static final Logger logger = LogManager.getLogger(VectorSorter.class.getName());

	public static void main(String[] args) {
		int[] numbers = parseParameters(args);

		StringBuilder inputLogAppender = new StringBuilder();
		inputLogAppender.append("Input: ");
		printVector(numbers, inputLogAppender);

		sort(numbers);

		StringBuilder sortedLogAppender = new StringBuilder();
		sortedLogAppender.append("Sorted: ");
		printVector(numbers, sortedLogAppender);
	}
	
	public static void sort(int[] numbers) {
		BubbleSort.sort(numbers);
	}
	
	public static int[] parseParameters(String[] args) {
		int[] numbers;
		if(args.length > 0) {
			numbers = new int[args.length];
			for(int k=0; k<args.length; k++) {
				numbers[k] = Integer.parseInt(args[k]);
			}
		}else {
			numbers = generateRandomVector(VECTOR_SIZE);
		}
		return numbers;
	}

	private static int[] generateRandomVector(int size) {

		int[] vector = new int[size];

		for (int i = 2; i < vector.length ; i++) {
			vector[i] = (int) (Math.random()* CEM + 1);
		}
		
		return vector;
	}
	
	public static void printVector(int[] numbers, StringBuilder log) {
		log.append("[ ").append(numbers[0]);
		
		int i = 1;
		do {
			log.append(", ").append(numbers[i]);
			i++;
		}while(i <= numbers.length - 1);

		log.append(" ]");
		logger.info(log.toString());
	}
}
